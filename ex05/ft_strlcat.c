/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gconejo- <gconejo-@student.42urduliz.com>  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 09:26:19 by gconejo-          #+#    #+#             */
/*   Updated: 2021/05/06 09:26:38 by gconejo-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_strlen(char *str)
{
	unsigned int	i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	char			*dst_aux;
	char			*src_aux;
	unsigned int	length;
	unsigned int	i;

	dst_aux = dest;
	src_aux = src;
	i = size;
	while (i-- != 0 && *dst_aux != '\0')
		dst_aux++;
	length = dst_aux - dest;
	i = size - length;
	if (i == 0)
		return (i + ft_strlen(src_aux));
	while (*src_aux != '\0')
	{
		if (i != 1)
		{
			*dst_aux++ = *src_aux;
			i--;
		}
		src_aux++;
	}
	*dst_aux = '\0';
	return (length + (src_aux - src));
}
