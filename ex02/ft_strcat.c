/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gconejo- <gconejo-@student.42urduliz.com>  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 09:24:48 by gconejo-          #+#    #+#             */
/*   Updated: 2021/05/06 09:25:15 by gconejo-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, char *src)
{
	char	*aux;
	int		i;
	int		j;

	aux = dest;
	i = 0;
	j = 0;
	while (aux[i] != '\0')
	{
		i++;
	}
	while (src[j] != '\0')
	{
		aux[i] = src[j];
		i++;
		j++;
	}
	aux[i] = '\0';
	return (dest);
}
