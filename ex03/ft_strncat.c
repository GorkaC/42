/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gconejo- <gconejo-@student.42urduliz.com>  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 09:25:27 by gconejo-          #+#    #+#             */
/*   Updated: 2021/05/06 09:25:41 by gconejo-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	char				*aux;
	unsigned int		i;
	unsigned int		j;

	aux = dest;
	i = 0;
	j = 0;
	while (aux[i] != '\0')
	{
		i++;
	}
	while (src[j] != '\0' && j < nb)
	{
		aux[i] = src[j];
		i++;
		j++;
	}
	aux[i] = '\0';
	return (dest);
}
