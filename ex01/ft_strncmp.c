/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gconejo- <gconejo-@student.42urduliz.com>  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 09:23:56 by gconejo-          #+#    #+#             */
/*   Updated: 2021/05/06 09:24:36 by gconejo-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	int	res;
	int	pos;

	pos = 0;
	res = 0;
	while (res == 0 && n > 0)
	{
		if (s1[pos] < s2[pos])
			res = -1;
		else if (s1[pos] > s2[pos])
			res = 1;
		if (s1[pos] == '\0')
			break ;
		pos++;
		n--;
	}
	return (res);
}
